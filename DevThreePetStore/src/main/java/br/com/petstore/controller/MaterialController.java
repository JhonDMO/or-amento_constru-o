package br.com.petstore.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.petstore.entity.Fornecedor;
import br.com.petstore.entity.Material;
import br.com.petstore.repository.FornecedorRepository;
import br.com.petstore.repository.MaterialRepository;

@RequestMapping("/lancarNotas")
@Controller
public class MaterialController {

	private MaterialRepository materialRepository;
	
	private FornecedorRepository fornecedorRepository;
	
	private List<Material> listaItens = new ArrayList<Material>();
	
	private List<Fornecedor> listaFornecedores = new ArrayList<Fornecedor>();
	
	

	@Autowired
    public MaterialController( MaterialRepository livroRepository, FornecedorRepository fornecedorRepository) {
          this.materialRepository = livroRepository;
          this.fornecedorRepository = fornecedorRepository;
          this.listaFornecedores = fornecedorRepository.findAll();
    }
	

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView paginaInicial(ModelMap model) {
		return new ModelAndView("home", model);
	}

	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String listaMateriais(Model model) {
		model.addAttribute("listaItens", this.listaItens);
		for (Fornecedor fornecedor : listaFornecedores) {
			System.out.println(fornecedor.getNome());
		}
		model.addAttribute("listaFornecedores", this.listaFornecedores);
		return "materiais";
	}
	
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public String adcionarMaterial(Material material) {
		try {
			materialRepository.save(material);
		} catch (Exception e) {
			System.out.println("ERRO AO CADASTRAR MATERIAL:" + e.getMessage());
		}
		return "redirect:/materiais";
	}
		
	@RequestMapping(value = "/livro/excluir", method = RequestMethod.POST)
	public String deletarLivroAutor(@RequestParam("codigoLivro") Long codigoLivro) {

		materialRepository.deleteById(codigoLivro);;
		return "redirect:/{autor}";
	}
	
	@RequestMapping(value ="/inserirMaterial" , method = RequestMethod.POST)
	public String inserirMaterial(@Valid Material material, BindingResult brMaterial, Model model) {
		if(brMaterial.hasErrors()) {
			model.addAttribute("listaItens", this.listaItens);
			return "materiais";
		}
		material.setValorTotal(calcValorTotal(material));
		listaItens.add(material);
		System.out.println("====================");
		System.out.println("====================");
		return "redirect:/lancarNotas/home";
	}

	private double calcValorTotal(Material material) {
		 material.setValorTotal(material.getValor_unitario() * material.getQuantidade());
		 return material.getValorTotal();
	}
}
