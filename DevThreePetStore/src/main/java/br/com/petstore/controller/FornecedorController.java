package br.com.petstore.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.petstore.entity.Fornecedor;
import br.com.petstore.repository.FornecedorRepository;
import br.com.petstore.repository.MaterialRepository;

@Controller
public class FornecedorController {

	private FornecedorRepository fornecedorRepository;
	private MaterialRepository materialRepository;

	@Autowired
	public FornecedorController(FornecedorRepository fornecedorRepository, MaterialRepository materialRepository) {
		this.fornecedorRepository = fornecedorRepository;
		this.materialRepository = materialRepository;
	}

	@RequestMapping(value = "/fornecedores", method = RequestMethod.GET)
	public String paginaInicialFornecedor(Model model) {
		List<Fornecedor> listaFornecedor = fornecedorRepository.findAll();
		if(listaFornecedor != null) {
			model.addAttribute("fornecedores", listaFornecedor);
		}
		return "/fornecedor/home_fornecedor";
	}
	
	@RequestMapping(value = "/fornecedores", method = RequestMethod.POST)
	public String adcionarFornecedor(Fornecedor fornecedor) {
		try {
			fornecedorRepository.save(fornecedor);
		} catch (Exception e) {
			System.out.println("ERRO AO CADASTRAR FORNECEDOR:" + e.getMessage());
		}
		return "redirect:/fornecedores";
	}
	
	@RequestMapping(value = "/fornecedor/excluir", method = RequestMethod.POST)
	public String deletarLivroAutor(@RequestParam("codigoLivro") Long codigoFornecedor) {		
		fornecedorRepository.deleteById(codigoFornecedor);
		//tratar, não é possivel deletar um fornecedor
		return "redirect:/fornecedores";
	}

}
