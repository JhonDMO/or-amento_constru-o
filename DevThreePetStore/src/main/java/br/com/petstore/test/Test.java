package br.com.petstore.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import br.com.petstore.entity.Fornecedor;
import br.com.petstore.entity.Material;
import br.com.petstore.repository.FornecedorRepository;

public class Test {
	public static void main(String[] args) {
		
		FornecedorRepository repository = null;
			
		Material m = new Material();
		Material m2 = new Material();
		Fornecedor f = new Fornecedor();
		
		m.setDesconto((double) 20);
		m.setDescricao("CIMENTO CP3");
		m.setNome("CIMENTO");
		m.setQuantidade(3);
		m.setValor_unitario(19.5);
		
		m2.setDesconto((double) 20);
		m2.setDescricao("CIMENTO CP3");
		m2.setNome("TIJOLO");
		m2.setQuantidade(3);
		m2.setValor_unitario(19.5);
		
		List<Material> listaMateriais = new ArrayList<>();
		listaMateriais.add(m);
		listaMateriais.add(m2);
		
		
		f.setBairro("teste");
		f.setCEP("454545");
		f.setCidade("rrtrtrt");
		f.setLogradouro("ererree");
		f.setNome("SARACURUNA");
		f.setNumero("3434");
		f.setObservacao("344545");
		
		for (Material material : listaMateriais) {
			System.out.println(material.getNome());
		}
		
		f.setMateriais(listaMateriais);
		
		try {
			repository.save(f);
		} catch (Exception e) {
			System.out.println("Ocorreu um erro ao salvar: " + e);
			e.printStackTrace();
		}

	}

}
