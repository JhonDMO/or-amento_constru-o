package br.com.petstore.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Fornecedor implements Serializable {	
	/**
	 * 
	 */
	private static final long serialVersionUID = -467551708058700032L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_Fornecedor;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="lista_materiais",
				joinColumns={@JoinColumn(name="id_fornecedor")},
				inverseJoinColumns={@JoinColumn(name="id_material")})
	private List<Material> materiais;
	
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@Column(name = "telefone_1", nullable = false)
	private String telefone_1;
	
	@Column(name = "telefone_2", nullable = false)
	private String telefone_2;
	
	@Column(name = "logradouro", nullable = false)
	private String logradouro;
	
	@Column(name = "numero", nullable = false)
	private String numero;
	
	@Column(name = "bairro", nullable = false)
	private String bairro;
	
	@Column(name = "cidade", nullable = false)
	private String cidade;
	
	@Column(name = "CEP", nullable = false)
	private String CEP;
	
	@Column(name = "observacao", nullable = false)
	private String observacao;

	public Long getId_Fornecedor() {
		return id_Fornecedor;
	}

	public void setId_Fornecedor(Long id_Fornecedor) {
		this.id_Fornecedor = id_Fornecedor;
	}

	public List<Material> getMateriais() {
		return materiais;
	}

	public void setMateriais(List<Material> materiais) {
		this.materiais = materiais;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone_1() {
		return telefone_1;
	}

	public void setTelefone_1(String telefone_1) {
		this.telefone_1 = telefone_1;
	}

	public String getTelefone_2() {
		return telefone_2;
	}

	public void setTelefone_2(String telefone_2) {
		this.telefone_2 = telefone_2;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCEP() {
		return CEP;
	}

	public void setCEP(String cEP) {
		CEP = cEP;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((CEP == null) ? 0 : CEP.hashCode());
		result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((id_Fornecedor == null) ? 0 : id_Fornecedor.hashCode());
		result = prime * result + ((logradouro == null) ? 0 : logradouro.hashCode());
		result = prime * result + ((materiais == null) ? 0 : materiais.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result + ((telefone_1 == null) ? 0 : telefone_1.hashCode());
		result = prime * result + ((telefone_2 == null) ? 0 : telefone_2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fornecedor other = (Fornecedor) obj;
		if (CEP == null) {
			if (other.CEP != null)
				return false;
		} else if (!CEP.equals(other.CEP))
			return false;
		if (bairro == null) {
			if (other.bairro != null)
				return false;
		} else if (!bairro.equals(other.bairro))
			return false;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (id_Fornecedor == null) {
			if (other.id_Fornecedor != null)
				return false;
		} else if (!id_Fornecedor.equals(other.id_Fornecedor))
			return false;
		if (logradouro == null) {
			if (other.logradouro != null)
				return false;
		} else if (!logradouro.equals(other.logradouro))
			return false;
		if (materiais == null) {
			if (other.materiais != null)
				return false;
		} else if (!materiais.equals(other.materiais))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (telefone_1 == null) {
			if (other.telefone_1 != null)
				return false;
		} else if (!telefone_1.equals(other.telefone_1))
			return false;
		if (telefone_2 == null) {
			if (other.telefone_2 != null)
				return false;
		} else if (!telefone_2.equals(other.telefone_2))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Fornecedor [id_Fornecedor=" + id_Fornecedor + ", materiais=" + materiais + ", nome=" + nome
				+ ", telefone_1=" + telefone_1 + ", telefone_2=" + telefone_2 + ", logradouro=" + logradouro
				+ ", numero=" + numero + ", bairro=" + bairro + ", cidade=" + cidade + ", CEP=" + CEP + ", observacao="
				+ observacao + "]";
	}
}
