package br.com.petstore.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

@Entity
public class Material {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_material;
	

	@ManyToMany(mappedBy="materiais")
	private List<Fornecedor> fornecedores;
	
	@Column(name = "nome", nullable = false)
	@NotNull(message=" O campo nome é obrigatório ")
	@Size(min=5)
	@Length(max=30, message="limite maximo de {max} caracteres.")
	private String nome;

	//enum
	@Column(name = "metrica", nullable = false)
	@NotEmpty(message=" O campo nome é obrigatório ")
	private String metrica;
	
	@Column(name = "descricao", nullable = true)
	@NotEmpty(message=" O campo nome é obrigatório ")
	@Length(max=150, message="limite maximo de {max} caracteres.")
	private String descricao;
	
	@Column(name = "quantidade", nullable = false)
	@NotNull(message=" O campo quantidade é obrigatório ")
	private int quantidade;
	
	@NotNull(message=" O campo valor unitario é obrigatório ")
	@Column(name = "valor_unitario", nullable = true)
	private Double valor_unitario;
	
	@Column(name = "desconto", nullable = true)
	private Double desconto;
	
	
	@Column(name = "valorTotal", nullable = true)
	private Double valorTotal;
	
	public Long getId() {
		return id_material;
	}

	public void setId(Long id) {
		this.id_material = id;
	}

	public Long getId_material() {
		return id_material;
	}

	public void setId_material(Long id_material) {
		this.id_material = id_material;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMetrica() {
		return metrica;
	}

	public void setMetrica(String metrica) {
		this.metrica = metrica;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public Double getValor_unitario() {
		return valor_unitario;
	}

	public void setValor_unitario(Double valor_unitario) {
		this.valor_unitario = valor_unitario;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
}

