package br.com.petstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jhon.oliveira
 *
 */
@SpringBootApplication
public class DevThreePetStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevThreePetStoreApplication.class, args);
	}
}

