package br.com.petstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.petstore.entity.Material;

public interface MaterialRepository extends JpaRepository<Material, Long> {

	List<Material> findByNome(String nome);

}
