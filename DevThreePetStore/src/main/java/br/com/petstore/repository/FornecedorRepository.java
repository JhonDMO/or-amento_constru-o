package br.com.petstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.petstore.entity.Fornecedor;

public interface FornecedorRepository extends JpaRepository<Fornecedor, Long> {

}
